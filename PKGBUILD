# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=(dhcp dhclient)
pkgbase=dhcp
pkgver=4.4.3
pkgrel=1
arch=('x86_64')
url="https://www.isc.org/dhcp/"
license=('custom:isc-dhcp')
groups=('base')
makedepends=('bash' 'iproute2')
source=(https://ftp.isc.org/isc/${pkgbase}/${pkgver}/${pkgbase}-${pkgver}.tar.gz
    dhclientat.service
    dhclient.conf
    dhcpd
    dhcpd.conf
    dhcpd.service)
sha256sums=(0e3ec6b4c2a05ec0148874bcd999a66d05518378d77421f607fb0bc9d0135818
    c29ea83f96eb5b15f95b087508a00d26b7a0cd49f17feafec77912ade57aa0ab
    5e151b624b0d1ab5abe8e3f088c91c37d1e02e8ad29aee8edb4fe20530e1d4a0
    c241a9ff80b1d03291290cc8f5d5c33df0fd166595cb418962c292791a2b08d4
    8ece4edb46b365a790e9de10078d78a5a399226ee18c4ff19a46bb5dda338304
    822e70b8c0511ea621384e2dc8b613b961698442b93c398f9407aa6d294d63dd)

build() {
    cd ${pkgbase}-${pkgver}

    export CFLAGS="${CFLAGS:--g -O2} -Wall -fno-strict-aliasing        \
           -D_PATH_DHCLIENT_SCRIPT='\"/usr/sbin/dhclient-script\"'     \
           -D_PATH_DHCPD_CONF='\"/etc/dhcp/dhcpd.conf\"'               \
           -D_PATH_DHCLIENT_CONF='\"/etc/dhcp/dhclient.conf\"'"

    ${configure}                                                \
        --sysconfdir=/etc/dhcp                                  \
        --localstatedir=/var                                    \
        --with-srv-lease-file=/var/lib/dhcpd/dhcpd.leases       \
        --with-srv6-lease-file=/var/lib/dhcpd/dhcpd6.leases     \
        --with-cli-lease-file=/var/lib/dhclient/dhclient.leases \
        --with-cli6-lease-file=/var/lib/dhclient/dhclient6.leases

    make -j1
}

package_dhcp(){
    pkgdesc="A DHCP server, client, and relay agent"
    depends=('glibc' 'dhclient')
    backup=(etc/dhcp/dhcpd.conf)
    install=${pkgbase}.install

    cd ${pkgbase}-${pkgver}

    make -C server DESTDIR=${pkgdir} install

    install -vDm644 ${srcdir}/dhcpd.conf ${pkgdir}/etc/dhcp/dhcpd.conf

    install -vdm755 ${pkgdir}/var/lib/dhcpd
    touch ${pkgdir}/var/lib/dhcpd/dhcpd.leases

    install -vDm644 ${srcdir}/dhcpd ${pkgdir}/etc/default/dhcpd
    install -vDm644 ${srcdir}/dhcpd.service ${pkgdir}/usr/lib/systemd/system/dhcpd.service

}

package_dhclient(){
    pkgdesc="A standalone DHCP client from the dhcp package"
    depends=('glibc' 'bash' 'iproute2')
    backup=(etc/dhcp/dhclient.conf)

    cd ${pkgbase}-${pkgver}

    make -C client DESTDIR=${pkgdir} install

    install -vm755 client/scripts/linux ${pkgdir}/usr/sbin/dhclient-script

    install -vDm644 ${srcdir}/dhclient.conf ${pkgdir}/etc/dhcp/dhclient.conf

    install -vdm755 ${pkgdir}/var/lib/dhclient

    install -vDm644 ${srcdir}/dhclientat.service ${pkgdir}/usr/lib/systemd/system/dhclient@.service
}
